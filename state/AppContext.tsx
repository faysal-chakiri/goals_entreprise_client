import React, { Dispatch, SetStateAction, useEffect, useState } from "react";

interface IAppContext {
  language: string;
  setLanguage: Dispatch<SetStateAction<string>>;
}

export const AppContext = React.createContext<IAppContext>({} as IAppContext);

export function AppProvider({ children }: any) {
  const [language, setLanguage] = useState<string>("fr");

  return (
    <AppContext.Provider
      value={{
        language,
        setLanguage,
      }}
    >
      {children}
    </AppContext.Provider>
  );
}
