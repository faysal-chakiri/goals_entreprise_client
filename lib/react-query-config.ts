import { QueryClient } from '@tanstack/react-query';

export const config = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: Infinity,
        },
    },
});
