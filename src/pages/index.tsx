import { useRouter } from "next/router";

import NavBar from "../../components/appBar/AppBar";
import { useTranslation } from "react-i18next";
import GoalContainer from "../../components/goals_entreprise/goalsContainer";

export default function Home() {
  const router = useRouter();
  const { t } = useTranslation();
  return (
    <>
      <NavBar />
      <GoalContainer />
    </>
  );
}
