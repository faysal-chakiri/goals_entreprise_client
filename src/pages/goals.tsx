import { NextPage } from "next";
import GoalContainer from "../../components/goals_entreprise/goalsContainer";

const PatientPage: NextPage = () => {
  return (
    <>
      <GoalContainer />
    </>
  );
};

export default PatientPage;
