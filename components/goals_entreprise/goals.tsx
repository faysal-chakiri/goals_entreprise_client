import {
  Box,
  Button,
  Container,
  Grid,
  IconButton,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import Image from "next/image";
import Logo from "../../public/images/logo_goals.png";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { useEffect, useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import CancelIcon from "@mui/icons-material/Cancel";

import {
  DataGrid,
  GridColDef,
  GridLoadIcon,
  GridToolbar,
} from "@mui/x-data-grid";
import React from "react";

export default function Goal() {
  const { t } = useTranslation();
  const [dataToShow, setDataToShow] = useState([]);
  const [idGoal, setIdGoal] = useState();
  const [openForm, setOpenForm] = useState(false);

  const typeGoals = [
    "Encourager l'auto formation",
    "Promouvoir la collaboration",
    "Encourager la spécialisation pro",
    "Encourager la Co-apprentissage Social",
  ];

  const getAllGoals = async () => {
    try {
      const { data } = await axios.get(`http://localhost:2000/goal/getAll`);
      console.log("data", data);
      setDataToShow(data);
    } catch (error) {
      console.error("Error fetching goals:", error);
    }
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  // const deleteGoal = async () => {
  //   try {
  //     const { data } = await axios.delete(
  //       `http://127.0.0.1:8000/api/goals/${idGoal}`
  //     );
  //     setIdGoal(undefined);
  //     getAllGoals();
  //   } catch (error) {
  //     console.error("Error deletiing goal:", error);
  //   }
  // };

  useEffect(() => {
    getAllGoals();
  }, []);

  const columns: GridColDef[] = [
    { field: "id", headerName: "Id", width: 150 },
    { field: "name_goal", headerName: "Nom de l'objectif", width: 150 },
    // { field: "Responsable", headerName: "Responsable", width: 150 },
    { field: "start_date", headerName: "Date de début", width: 150 },
    { field: "end_date", headerName: "Date de fin", width: 150 },
    { field: "type_goal", headerName: "Type d'objectif", width: 150 },
    // { field: "start_date", headerName: "Template", width: 150 },
    // { field: "nbr_kpi", headerName: "Nombre des KPI", width: 150 },
    // { field: "no_members", headerName: "No.members", width: 150 },
  ];

  const fiels = [
    { name: "name_goal", label: "Nom de l'objectif" },
    { name: "start_date", label: "Date de début" },
    { name: "end_date", label: "Date de fin" },
    { name: "type_goal", label: "Type d'objectif" },
    // { name: "Responsable", label: "Responsable" },
    // { name: "nbr_kpi", label: "Nombre des KPI" },
    // { name: "no_members", label: "No.members" },
  ];

  const initialState = React.useMemo(
    () => ({
      ...dataToShow.initialState,
      pagination: { paginationModel: { pageSize: 10 } },
    }),
    [dataToShow.initialState]
  );

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log(data.get("first_name"));
    try {
      await axios.post(`http://127.0.0.1:8000/api/goals`, data);
    } catch (error) {
      console.error("Error deletiing goal:", error);
    }
    // mutate({
    //   first_name: data.get("first_name") as string,
    //   last_name: data.get("last_name") as string,
    //   number_phone: parseInt(data.get("number_phone") as any),
    //   email: data.get("email") as string,
    //   password: data.get("password") as string,
    //   is_active: 1,
    //   is_interested: isInterested ? 1 : 0,
    // });
  };

  return (
    <Container maxWidth="xl">
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Grid item width="100%">
          <Paper
            sx={{
              bgcolor: "#44B3E7",
              mt: "1rem",
              mb: "2rem",
            }}
          >
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item>
                <Image
                  src={Logo}
                  alt=""
                  width={60}
                  height={60}
                  style={{ margin: "0.6rem" }}
                />
              </Grid>
              <Grid item>
                <Typography variant="h6" component="h2" color="#E95A0C">
                  Goals
                </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item>
          <Box sx={{ width: "100%" }}>
            <Button
              variant="contained"
              startIcon={<AddIcon />}
              sx={{ mb: "1rem" }}
              onClick={() => setOpenForm(true)}
            >
              {t("common.buttonAdd")}
            </Button>

            {idGoal && (
              <Button
                variant="contained"
                startIcon={<DeleteIcon />}
                sx={{ mb: "1rem", ml: "1rem" }}
                // onClick={() => deleteGoal()}-
                color="error"
              >
                {t("common.buttonDelete")}
              </Button>
            )}

            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="flex-start"
              spacing={2}
            >
              <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
                {openForm && (
                  <Grid item maxWidth="25em">
                    <Paper elevation={3}>
                      <Grid
                        container
                        direction="column"
                        justifyContent="center"
                        alignItems="center"
                      >
                        {fiels.map((field, index: number) => (
                          <Grid item key={index}>
                            <TextField
                              margin="normal"
                              required
                              name={field.name}
                              label={field.label}
                              sx={{ m: "1em" }}
                            />
                          </Grid>
                        ))}
                      </Grid>
                      <Button
                        variant="contained"
                        startIcon={<AddIcon />}
                        sx={{ m: "1rem" }}
                        type="submit"
                      >
                        {t("common.buttonAdd")}
                      </Button>
                      <Button
                        variant="contained"
                        startIcon={<CancelIcon />}
                        sx={{ m: "1rem" }}
                        onClick={() => setOpenForm(false)}
                        color="warning"
                      >
                        {t("common.buttonCancel")}
                      </Button>
                    </Paper>
                  </Grid>
                )}
              </Box>

              <Grid item>
                <DataGrid
                  rows={dataToShow}
                  columns={columns}
                  {...dataToShow}
                  slots={{ toolbar: GridToolbar }}
                  initialState={initialState}
                  experimentalFeatures={{ ariaV7: true }}
                  onCellClick={(e: any) => setIdGoal(e.id)}
                  checkboxSelection
                />
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
